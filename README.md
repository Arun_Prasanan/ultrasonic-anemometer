# Ultrasonic Anemometer
## Introduction
Cup anemometers are the most used type of anemometers in the world, they rely on the roratary motion imparted to an axle connected to cups that catch the force of wind. The rotational speed of the axle corresponds to the live wind speed at the installed location. Still, these anemometers are prone to some limitations namely the wear & tear of the friction bearings of the axle. In addition to that, in temperate regions, during the winter the anemometer freezes and ceases to collect any viable data. Our objective was to develop a low cost non mechanical form of anemometer that could overcome the forestated limitations. Out of the various methods which exists for measuring windspeed non-mechanically, we chose ultrasonic method for its faster response times and precision. 

<img src="https://gitlab.com/akhilpsicfoss/ultrasonic-anemometer/-/raw/ec417092e790c5f91062b0648358d275b0645364/Images/outdoor%20test.jpg" >

## List of Parameters recorded
### Wind velocity and Wind Direction
* Duration measurement

## Prerequisites
* Arduino IDE [Tested]

## Components used
### Sensor used

1. [MULTICOMP PRO Ultrasonic Sensor](https://robu.in/product/multicomp-pro-ultrasonic-sensor-transceiver/)
<img src="https://robu.in/wp-content/uploads/2022/09/jfhgkdf-figdfj.jpg" width="200" height="200"> 

## Data Acquisition Devices (DAQs)
1. [Sparkfun Anemometer](https://www.mouser.in/ProductDetail/SparkFun/SEN-15901?qs=YwPsRIUVAOdCNLFQoffCEg%3D%3D&mgh=1&utm_id=18173878537&gad_source=4&gclid=CjwKCAjwm_SzBhAsEiwAXE2Cv8gh1-cq3TdG3rz0WBKghLlp5VD7eA1jHGSoyp0_aIhW359w-SveexoCXSgQAvD_BwE) -The mechanical anemometer, used for measuring wind velocity and direction, serves as a reference for comparing wind measurements against ultrasonic readings
<img src="https://encrypted-tbn0.gstatic.com/shopping?q=tbn:ANd9GcSLd2AGt2pXtn-_7BpCcy47UFhg87WjSesypTZTrv0F9Ox1SUURDpY4xH6JtRUtLbN2j_onRlGVLpFcWhU3lXTcwLoBwrHyZ835wSZN0x81ypIZuB51haTCug&usqp=CAE" align="center" width="300" height="300">

2. [Digital Anemometer](https://www.amazon.in/Digital-Anemometer-Handheld-Accurately-Temperature/dp/B07J64TCBQ?source=ps-sl-shoppingads-lpcontext&ref_=fplfs&psc=1&smid=A3LD4MMBBTHP7M) -portable device used to measure wind speed and, in some models, wind direction, temperature, and other weather-related parameters. It features a digital display for easy reading of measurements
<img src="https://encrypted-tbn1.gstatic.com/shopping?q=tbn:ANd9GcQCkYWL_pip3kVeebTBZLPPHAdraBnwKcYwBW6pwOgxim3Y6jrM0a56bj-4CJHn4nKltFhode1O2q-UbCtpAqWRoEMQta2usdhTCJpJOzjQMlN8M9ekzs86&usqp=CAE" align="center" width="300" height="300">

3. [Arduino nano ble 33 sense](https://www.electronicscomp.com/arduino-nano-33-ble-sense-with-header-rev-2?gad_source=4&gclid=CjwKCAjwm_SzBhAsEiwAXE2Cv6TfU9c9qHBtF7Ikwa9ur35FpB2n8hJQZIlFClJrvnU0PfiYgABy2RoC6L4QAvD_BwE) 
<img src="https://thinkrobotics.com/cdn/shop/files/ABX00070_01.iso_720x.jpg?v=1704274473" align="center" height="300" width="300">

## Getting started
* Make sure that you have an [Arduino nano ble 33 sense](https://www.electronicscomp.com/arduino-nano-33-ble-sense-with-header-rev-2?gad_source=4&gclid=CjwKCAjwm_SzBhAsEiwAXE2Cv6TfU9c9qHBtF7Ikwa9ur35FpB2n8hJQZIlFClJrvnU0PfiYgABy2RoC6L4QAvD_BwE) and Ultrasonic transducers.
* Connect the Components as shown in the [wiring diagram](https://gitlab.com/akhilpsicfoss/ultrasonic-anemometer/-/blob/6f85e9dea8601c8e586d519b57ce81b2f65bd902/Hardware/Circuit%20Diagram%20Ultrasonic%20Anemometer.pdf)
* [TC74HC4052AP Multiplexer](https://gitlab.com/akhilpsicfoss/ultrasonic-anemometer/-/blob/main/Hardware/multiplexer.png) needed for switching circuit.
* Install Arduino IDE.
* Select board : Arduino nano ble 33
* Refer the [documentation](https://gitlab.com/akhilpsicfoss/ultrasonic-anemometer/-/blob/main/Documentation/Development_of_Inhouse_Non_Mechanical_Anemometer_.pdf?ref_type=heads) .

## Results
Wind Data is available at serial port of the connected terminal in the data Format as Date, T N-S, T S-N, T E-W , T W-E, Wind Speed, Wind Direction respectively.
<img src="https://gitlab.com/akhilpsicfoss/ultrasonic-anemometer/-/raw/main/Images/anemometer_output_arduino.png"> 

After preprocessing the [dataset](https://gitlab.com/akhilpsicfoss/ultrasonic-anemometer/-/blob/main/Dataset/analysis%20ultrasonic%20anemometer%20-%20A.csv),the accuracy and error rate were calculated, yielding a **72.62% accuracy** for the ultrasonic anemometer and **27.38% error rate** as compared with Sparkfun anemometer.


## Project Members
* [Akhil Prasad Sebastian](https://gitlab.com/akhilpsicfoss)
* [Shruthi G](https://gitlab.com/shruthigirija)
* [Arun P](https://gitlab.com/Arun_Prasanan)
* Anantha Krishnan

## License
This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/akhilpsicfoss/ultrasonic-anemometer/-/blob/6f85e9dea8601c8e586d519b57ce81b2f65bd902/LICENSE) file for details.
## Contact Information
You can find us at:\
ICFOSS\
Greenfield Stadium,\
Opposite University of Kerala Campus,\
Karyavattom,Thiruvananthapuram,\
Kerala 695581

For more information, visit our [website](https://icfoss.in/).