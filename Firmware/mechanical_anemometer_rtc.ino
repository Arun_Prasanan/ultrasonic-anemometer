#include "RTClib.h"
#define Reed_pin  2
#define pot_pin A0
unsigned long previousTime = 0;
volatile int count = 0;
float ws = 0;
float dir_Value = 0;

RTC_DS1307 rtc;
String Time;

void setup()
{
  Serial.begin(115200);
  pinMode(Reed_pin, INPUT_PULLUP);
  Serial.print("Initializing RTC...");
  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    Serial.flush();
    while (1);
  }
  Serial.println("RTC initialization done.");

  attachInterrupt(digitalPinToInterrupt(Reed_pin), wind_count, FALLING);

}


void loop()
{DateTime time = rtc.now();
  Time = time.timestamp(DateTime::TIMESTAMP_FULL);

  {
    if (previousTime > millis())
      previousTime = 0;

    else if ((millis() - previousTime) > 500)
    {
      previousTime = millis();
      
  Serial.print(Time);
  Serial.print(",");
      Serial.print(ws);
      Serial.print(",");
      int dir_Value = analogRead(A0);
      Serial.println(dir_Value);
      count = 0;
      ws = 0;

    }
  }}


  void wind_count()
  {
    count++;
    ws = count * (2.4 / 2);
    //ws=count * 1.609 * (2.4 / 2);  //for mechanical anemometer (2.4), *tion with 1.609 gives mph to kmph.
  }
